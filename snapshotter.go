package snapshotter

import (
	"log"
	"time"
)

type Snapshotter struct {
	init    bool
	storage Storage
	hasher  Hasher
	filter  Filter
	differ  Differ
	log     *log.Logger
}

func New(storage Storage, hasher Hasher, filter Filter, logger *log.Logger) *Snapshotter {
	if logger == nil {
		logger = log.Default()
	}
	return &Snapshotter{
		storage: storage,
		hasher:  hasher,
		filter:  filter,
		log:     logger,
	}
}

func (t *Snapshotter) Load() (err error) {
	var state map[string]interface{}
	if state, err = t.storage.Load(); err != nil {
		return
	}
	if state == nil {
		state = map[string]interface{}{}
	}

	t.log.Println("snapshot: load:", len(state))
	t.differ = NewDiffer(state, t.hasher, t.filter)
	t.init = len(state) == 0
	return nil
}

func (t *Snapshotter) Take(ts time.Time, data interface{}, onChange func(state map[string]interface{}, added, removed []string, changed []Change)) error {
	added, removed, changed, err := t.differ.Update(data)
	if err != nil {
		t.log.Println("snapshot: diff: unexpected error:", err)
		return err
	}

	if t.init && len(added) != 0 {
		added = nil
	}

	if !t.init && len(added) == 0 && len(removed) == 0 && len(changed) == 0 {
		return nil
	}

	state := t.differ.State()
	if onChange != nil {
		onChange(state, added, removed, changed)
	}

	if err = t.storage.Save(ts, state); err != nil {
		t.log.Println("snapshot: unexpected error:", err)
		return err
	}

	t.init = false
	t.log.Println("save:", len(state))
	return nil
}
