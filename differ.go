package snapshotter

import (
	"errors"
	"reflect"

	"github.com/r3labs/diff"
)

type Differ interface {
	Update(data interface{}) (added, removed []string, changed []Change, err error)
	State() map[string]interface{}
}

type Change struct {
	ID string
	diff.Change
}

type Hasher func(interface{}) string

type Filter func(change Change) bool

type differ struct {
	hash  Hasher
	state map[string]interface{}
	match Filter
}

func none(change Change) bool {
	return true
}

func NewDiffer(init map[string]interface{}, hash Hasher, filter Filter) Differ {
	if filter == nil {
		filter = none
	}
	return &differ{
		hash:  hash,
		state: init,
		match: filter,
	}
}

func (t *differ) Update(data interface{}) (added, removed []string, changed []Change, err error) {
	missing := map[string]struct{}{}
	for k := range t.state {
		missing[k] = struct{}{}
	}

	val := reflect.ValueOf(data)
	switch val.Kind() {
	case reflect.Array, reflect.Slice:
		for i := 0; i < val.Len(); i++ {
			v := val.Index(i).Interface()
			id := t.hash(v)
			delete(missing, id)

			l := t.state[id]
			if l == nil {
				t.state[id] = v
				added = append(added, id)
				continue
			}

			var ch diff.Changelog
			if ch, err = diff.Diff(v, l); err != nil {
				return nil, nil, nil, err
			} else if len(ch) == 0 {
				continue
			}

			for _, c := range ch {
				cc := Change{
					ID:     id,
					Change: c,
				}
				if !t.match(cc) {
					continue
				}

				changed = append(changed, cc)
			}

			t.state[id] = v
		}
	case reflect.Map:
		i := val.MapRange()
		for i.Next() {
			id := i.Key().String()
			v := i.Value().Interface()
			if t.hash != nil {
				id = t.hash(v)
			}
			delete(missing, id)

			l := t.state[id]
			if l == nil {
				t.state[id] = v
				added = append(added, id)
				continue
			}

			var ch diff.Changelog
			if ch, err = diff.Diff(v, l); err != nil {
				return nil, nil, nil, err
			} else if len(ch) == 0 {
				continue
			}

			for _, c := range ch {
				cc := Change{
					ID:     id,
					Change: c,
				}
				if !t.match(cc) {
					continue
				}

				changed = append(changed, cc)
			}

			t.state[id] = v
		}
	default:
		err = errors.New("only map, array and slice are supported")
		return
	}

	for id := range missing {
		delete(t.state, id)
		removed = append(removed, id)
	}

	return
}

func (t *differ) State() map[string]interface{} {
	return t.state
}

func Key(key string) Hasher {
	return func(value interface{}) string {
		return value.(map[string]interface{})[key].(string)
	}
}

func ID(value interface{}) string {
	return value.(map[string]interface{})["id"].(string)
}
