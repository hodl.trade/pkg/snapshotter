package snapshotter

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"time"
)

type Storage interface {
	Load() (map[string]interface{}, error)
	Save(time.Time, map[string]interface{}) error
}

func NewFileStorage(dir string, state string) Storage {
	return &fileStorage{
		dir:   dir,
		state: state,
	}
}

type fileStorage struct {
	dir   string
	state string
}

func (t *fileStorage) Load() (state map[string]interface{}, err error) {
	var f *os.File
	if f, err = os.Open(t.state); err != nil && os.IsNotExist(err) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&state)
	return state, err
}

func (t *fileStorage) Save(ts time.Time, state map[string]interface{}) (err error) {
	var f, s *os.File

	if f, err = os.OpenFile(t.state, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644); err != nil {
		return
	}
	defer f.Close()

	if err = os.MkdirAll(t.dir, 0755); err != nil {
		return err
	}

	if s, err = os.OpenFile(filepath.Join(t.dir, ts.UTC().Format(time.RFC3339)+".json"), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644); err != nil {
		return
	}
	defer s.Close()

	e := json.NewEncoder(io.MultiWriter(f, s))
	e.SetIndent("", "  ")
	if err = e.Encode(state); err != nil {
		return
	}

	return nil
}
